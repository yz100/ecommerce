//
//  CategoryPrototypeCell.swift
//  Mobile Store
//
//  Created by Ashwinkarthik Srinivasan on 3/27/15.
//  Copyright (c) 2015 Yun Zhang. All rights reserved.
//

import UIKit

class CategoryPrototypeCell: UICollectionViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    
    @IBOutlet weak var categoryTitle: UILabel!
    
    
    
}
